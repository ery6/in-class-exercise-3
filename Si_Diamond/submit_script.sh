#!/bin/bash
#SBATCH --job-name=ME511
#SBATCH --nodes 1
#SBATCH --ntasks-per-node=20
#SBATCH --cpus-per-task=2
#SBATCH -q long
#SBATCH -t 5:00:00

# The following lines set up all environment variables consistently.
# You need these.
module load FHIaims
ulimit -s unlimited

export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1
export MKL_DYNAMIC=FALSE

srun -n $SLURM_NTASKS aims.231208.scalapack.mpi.x < /dev/null > aims.out 2>&1
